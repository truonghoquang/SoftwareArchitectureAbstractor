Hướng dẫn cài đặt và sử dụng phần mềm SAAbs

1. Cài đặt JRE
- Do một số lý do kỹ thuât, phần mềm cần được chạy trên JRE version 1.8.0_25 (JRE 8u25)
- Download JRE 8u25 ở link sau: http://www.oracle.com/technetwork/java/javase/downloads/java-archive-javase8-2177648.html#jre-8u25-oth-JPR

2. Cài đặt Graphviz
- Download Graphviz phiên bản mới nhất ở link sau: http://www.graphviz.org/Download..php 
- Cài đặt Graphviz đơn giản bằng cách click vào file cài đặt vừa download và làm theo hướng dẫn. 

3. Download và cài đặt Visual Paradigm (Để tạo file xmi từ source code)
- Download trial version (sử dụng miễn phí trong 30 ngày) từ trang web: http://www.visual-paradigm.com/download/
- Cài đặt theo hướng dẫn (chọn phiên bản Enterprise để có chức năng reverse engineering)

4. Sinh file .xmi từ source code sử dụng chức năng Instant reverse của Visual Paradigm
- Chọn Tools > Code > Instant Reverse...
- Trong cửa sổ Instant Reverse:
	+ chọn ngôn ngữ của mã nguồn (Java, C++)
	+ Click Add Source Folder -> Chọn thư mục chứa mã nguồn
	+ Bỏ chọn "Click on Demand"
	+ Chọn OK để bắt đầu quá trình reverse engineering
* Kết qủa của quá trình trên là một class diagram, giờ ta cần lưu class diagram đó ra định dạng .xmi như sau:

- Chọn Project > Export > XMI... từ toolbar.
- Trong cửa sổ Export XMI:
	+ Chọn đường dẫn để lưu file .XMI.
	+ Cấu hình định dạng file XMI (khuyến nghị chọn version 1.2 hoặc 1.0)
	+ Chọn nút OK để hoàn thành quá trình. 
* Kết quả của quá trình này là file XMI lưu trữ reverse engineering class diagram của mã nguồn. Phần mềm SAAbs có nhiệm vụ biểu diễn class diagram đó dưới nhiều mức độ phức tạp khác nhau. Quá trình sử dụng phần mềm được trình bày ở bước 5.
* (Chi tiết hơn về quá trình trên ở các link sau: http://www.visual-paradigm.com/support/documents/vpuserguide/276/277/28011_reverseengin.html và http://www.visual-paradigm.com/support/documents/vpuserguide/124/221/6863_exportingxmi.html)

5. Download phần mềm SAAbs và kiểm tra xem phần mềm hoạt động bình thường hay không
- Phần mềm có thể được download tại: https://gitlab.com/truonghoquang/SoftwareArchitectureAbstractor
- Unzip file vừa download vào thư mục mong muốn.
- Khởi động phần mềm từ DOS (cần thiết) bằng các câu lệnh sau
	cd [Folder_SoftwareArchitectureAbstractor]
	java -jar ReverseDesigner.jar
- Sau bước này, màn hình chính của phần mềm sẽ xuất hiện.

6. Sử dụng phần mềm SAAbs (Software Architecture Abstractor)
- Chọn version XMI phù hợp: Settings > XMI > UML... (Ví dụ, chọn UML 1.x and XMI 1.1-1.3 để phù hợp với file xuất theo XMI 1.2).
- Load XMI file: File > Load XMI File : Chọn đường dẫn đến file XMI đã tạo được từ bước 4 (Một vài file XMI/XML mẫu được lưu trong thư mục samples)
Kết quả của bước này là một cấu trúc code dạng cây ở phía trái màn hình chính 

- Trên cây đó, tích chọn một vài class quan trọng nhất (khoảng 20% số class là đủ - phần mềm sẽ tự đánh giá mức độ quan trọng của các lớp còn lại dựa trên dữ liệu này)
- Sau khi đã chọn xong, nhấn nút Analyse
- Sau khi kết thúc bước này, nhấn nút Class Diagram ở góc dưới bên phải để hiện màn hình đồ hoạ
- Trên màn hình đồ hoạ biểu diễn class diagram, dùng thanh trượt để quyết định mức độ phức tạp, và chọn chế độ hiển thị khác nhau trong mục Settings > Ranking Classes Display.

Video hướng dẫn cụ thể về cách sử dụng phần mềm có thể xem tại: https://www.youtube.com/watch?v=dHBB5wA2wDI 

Mọi thông tin và thắc mắc về quy trình trên xin liên hệ:

Hồ Quang Trường
Trường đại học Chalmers, Thuỵ Điển
Email: truongh@chalmers.se, hoặc truonghoquang@gmail.com
skype: hoquangtruong (Vinh, Vietnam)

XIN CHÂN THÀNH CẢM ƠN!